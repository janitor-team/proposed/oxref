#include "main.ih"

void header(int argc, char **argv)
{
    Arg &arg = Arg::instance();

    cout << 
        arg.basename() << " by " << Icmbuild::author << '\n' <<
        arg.basename() << " V" << Icmbuild::version << ' ' << 
                                                    Icmbuild::years << "\n"
        "\n"
        "CREATED " << DateTime().rfc2822() << "\n"
        "CROSS REFERENCE FOR: ";

    copy(argv + 1, argv + argc, ostream_iterator<char const *>(cout, " "));

    cout << "\n"
            "\n";
}

