Source: oxref
Section: utils
Priority: optional
Maintainer: Frank B. Brokken <f.b.brokken@rug.nl>
Uploaders: George Danchev <danchev@spnet.net>,
           tony mancill <tmancill@debian.org>
Build-Depends: debhelper-compat (= 13),
               libbobcat-dev (>= 5.07.00),
               icmake (>= 9.03.01),
               yodl (>= 4.02.02)
Standards-Version: 4.6.1
Vcs-Browser: https://salsa.debian.org/debian/oxref
Vcs-Git: https://salsa.debian.org/debian/oxref.git
Homepage: https://fbb-git.gitlab.io/oxref
Rules-Requires-Root: no

Package: oxref
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}, binutils
Description: cross reference utility
 The program oxref writes to the standard output stream a cross reference of
 symbols defined in unstripped object files and/or libraries.
 .
 It demangles C++ symbols and can (probably) be used for a large range of
 languages.
 .
 Oxref does not interpret the object files and libraries itself, but calls
 objdump(1) to do so. The output produced by objdump is then filtered by
 oxref, generating the cross reference listing.
