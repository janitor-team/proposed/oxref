#include "storage.ih"

Storage::Storage()
:
    d_arg(Arg::instance())
{
    string spec;                            // process all 'R' replacements
    for (size_t idx = 0, end = d_arg.option('R'); idx != end; ++idx)
    {
        d_arg.option(idx, &spec, 'R');      // get the spec
        storeReplacement("-R option", spec);    // store it.
    }

    if (d_arg.option(&spec, 'r'))           // replacements file?
        replacementFile(spec);                  // then process its replacements
}
