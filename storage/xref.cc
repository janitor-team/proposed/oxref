#include "storage.ih"

void Storage::xref() const
{
    if (d_arg.option('X'))          // no xref info requested
        return;

    cout << 
        setfill('-') << setw(70) << '-' << setfill(' ') << "\n"
        "CROSS REFERENCE LISTING:\n"
        "\n" <<
        d_store << '\n';        // store's insertion operator shows the
                                // xreference info
}
