#include "storage.ih"

void Storage::tree() const
{
    string symbol;

    if (d_arg.option(&symbol, 't'))     // tree start symbol specified
        d_store.tree(symbol);
}

