#ifndef INCLUDED_STORAGE_
#define INCLUDED_STORAGE_

#include <iosfwd>
#include <string>
#include <vector>

#include <bobcat/pattern>

#include "../store/store.h"

// see store.h for a description of the data-storage organization

namespace FBB
{
    class Arg;
}

class Storage
{
    static FBB::Pattern s_reject;       // rejected lines from objdump
    static FBB::Pattern s_objFile;
    static FBB::Pattern s_abs;
    static FBB::Pattern s_UND;
    static FBB::Pattern s_g_F;
    static FBB::Pattern s_g_O;

    FBB::Arg const &d_arg;
    Store d_store;

        // input lines 'first' text is replaced by 'second'.
        // Format: XfirstXsecondX where X is a separator char    
    std::vector<std::pair<std::string, std::string>> d_replacement;

    public:
        typedef std::string value_type;
        typedef value_type const &const_reference;

        Storage();
                                    // interpret lines from objdump
                                    // using the functions below
        void push_back(std::string line);

        void xref() const;          // show the cross-reference info
        void tree() const;          // show the call-tree of symbol -t

    private:
        void function(std::string const &symbol);    
        void objFile(std::string const &fname);
        void sourceFile(std::string const &fname);
        void undefined(std::string const &symbol);
        void object(std::string const &symbol);

        void replacements(std::string &line) const; // perform replacements
                                                    // in d_replacement
        void replacementFile(std::string const &fname);
        void storeReplacement(std::string const &what, std::string const &spec);
};

#endif
