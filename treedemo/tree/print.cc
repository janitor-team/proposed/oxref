#include "tree.ih"

void Tree::print(string const &name, size_t level)
{
    cout << name;

    for (size_t idx = 0; idx != level; ++idx)
    {
        if (d_info[idx].name == name)
        {
            cout << " ==> " << idx << '\n';
            return;
        }
    }

    cout << '\n';    

    size_t size = d_idMap.callSize(name);

    d_info.resize(level + 1);

    d_info[level] = Data{ size, 0, name };

    size_t nextLevel = level + 1;   // next recursion

    for (size_t elem = 0; elem != size; ++elem)
    {
        ++d_info[level].current;
        indent(level);
        print(d_idMap.element(elem, name), nextLevel);
    }
}
