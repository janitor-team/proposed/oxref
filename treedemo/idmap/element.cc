#include "idmap.ih"

string const &IDmap::element(size_t idx, string const &name) const
{
    auto iter = find(name);

    if (iter == end())
        throw runtime_error{ "No entry `" + name + '\'' };

    return iter->second[idx];
}
            
