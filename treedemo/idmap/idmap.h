#ifndef INCLUDED_IDMAP_
#define INCLUDED_IDMAP_

#include <string>
#include <vector>
#include <unordered_map>
#include <initializer_list>

class IDmap: 
        private std::unordered_map< std::string, std::vector<std::string> >
{
    public:
        IDmap();

        size_t callSize(std::string const &name) const;

                                        // no idx bound check
        std::string const &element(size_t idx, std::string const &name) const;

        void add(std::string const &name, 
                 std::initializer_list<std::string> &&calls);
};

#endif
