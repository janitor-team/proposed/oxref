#include "idmap.ih"

size_t IDmap::callSize(std::string const &name) const
{
    auto iter = find(name);

    if (iter == end())
        throw runtime_error{ "No entry `" + name + '\'' };

    return iter->second.size();
}

