#include "store.ih"

// static
bool Store::findSymbol(XrefData const &data, string const &target)
{
    return  not data.sourceFile().empty()           // source file
            and                                     // and
            data.symbol() == target;                // found the target symbol
}
